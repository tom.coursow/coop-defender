// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CDWeapon.h"
#include "CDProjectile.h"
#include "CDProjectileWeapon.generated.h"

class ACDProjectile;

/**
 * 
 */
UCLASS()
class COOPDEFENDER_API ACDProjectileWeapon : public ACDWeapon
{
	GENERATED_BODY()

public:
	ACDProjectileWeapon();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ACDProjectile> ProjectileClass;
	
	/**
	 * Handles Projectile Based Weapon Logic by spawning a ACDProjectile which takes care of collision and damage dealing
	 */
	virtual void OnFire_Implementation(AActor* WeaponOwner, FVector EyeLocation, FRotator EyeRotation, FVector AimLocation) override;
};
