// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArchiveReplaceOrClearExternalReferences.h"
#include "Camera/CameraShake.h"
#include "CDWeapon.generated.h"

class USkeletalMeshComponent;
class USceneComponent;
class UDamageType;
class USoundBase;
class UParticleSystem;

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	NONE		UMETA(DisplayName = "None"),
	PISTOL		UMETA(DisplayName = "Pistol"),
	RIFLE		UMETA(DisplayName = "Rifle"),
	GRENADE 	UMETA(DisplayName = "Grenade")
};

UCLASS()
class COOPDEFENDER_API ACDWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACDWeapon();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	USceneComponent* SceneRoot;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float FireCooldown;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Weapon")
	float CurrentFireCooldown;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	bool Automatic;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	int32 AmmoMagazineSize;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Weapon")
	int32 CurrentAmmo;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EAmmoType AmmoType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float ReloadTime;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Weapon")
	float CurrentReloadTime;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UDamageType> DamageType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	USoundBase* FireSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* MuzzleEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* DefaultHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* FleshDefaultHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* FleshVulnerableHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	UParticleSystem* TracerEffect;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	FName MuzzleSocketName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<UCameraShake> FireShake;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float BaseDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float WeakPointDamageMultiplier;

private:
	UFUNCTION()
	void UpdateReload();
	
protected: // FUNCTIONS
	virtual void BeginPlay() override;
	/**
	 * Handles the weapon specific fire logic.
	 * For example a Sniper Rifle could use a line trace, but a Rocket Launcher could spawn a Rocket Projectile.
	 * The ACDWeapon Base class uses a line trace based approach and applies damage to the actor which it hit.
	 */
	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnFire(AActor* WeaponOwner, FVector EyeLocation, FRotator EyeRotation, FVector AimLocation);
	virtual void OnFire_Implementation(AActor* WeaponOwner, FVector EyeLocation, FRotator EyeRotation, FVector AimLocation);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void PlayFireEffects();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void PlayHitEffects(EPhysicalSurface SurfaceType, FVector ImpactLocation);

	/**
	 * Gets the Weapons SkeletalMesh Muzzle Socket Location (world space)
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	FVector GetMuzzleSocketLocation();
	
public: // FUNCTIONS
	/**
	 * Fires the weapon if the weapon is currently used by an actor.
	 * Will play the fire sound and fire muzzle effect if assigned as well.
	 * This function calls OnFire() for the weapon specific fire logic
	 */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void Fire();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void Reload();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void OnReloadFinished();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	bool IsReloading() const;

	virtual void Tick(float DeltaSeconds) override;

	FVector GetAimLocation(FVector EyeLocation, FRotator EyeRotation);
	bool IsAutomatic() const;
	EAmmoType GetAmmoType() const;
	int32 GetCurrentAmmo() const;
	void SetCurrentAmmo(int32 NewAmmo);
	int32 GetAmmoMagazineSize() const;
	
};