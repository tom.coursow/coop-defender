// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/Material.h"
#include "CDExplodable.generated.h"

class UCDHealthComponent;
class USphereComponent;
class UStaticMeshComponent;

UCLASS()
class COOPDEFENDER_API ACDExplodable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for thais actor's properties
	ACDExplodable();

protected: // VARIABLES
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Explodable")
	UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Explodable")
	USphereComponent* ExplosionArea;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Explodable")
	UCDHealthComponent* HealthComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explodable")
	UParticleSystem* ExplosionEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explodable")
	USoundBase* ExplosionSound;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Explodable")
	bool ExplodeOnDamage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Explodable")
	float ExplosionForce;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Explodable")
	float ExplosionDamage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explodable")
	const UDamageType* DamageType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explodable")
	UMaterialInterface* DefaultMaterial;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explodable")
	UMaterialInterface* ExplodedMaterial;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Explodable")
	bool Exploded;

protected: // FUNCTIONS
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
	void OnHealthChanged(UCDHealthComponent* HealthComp, float Health, float HealthDelta, const UDamageType* DamageTypeSettings, AController* InstigatedBy, AActor* DamageCauser);
	UFUNCTION()
	void OnExplosionAreaBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	

	void Explode();

};
