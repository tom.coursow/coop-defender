// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CDWeapon.h"
#include "CDCharacter.generated.h"

class UCDHealthComponent;
class ACDWeapon;
class USpringArmComponent;
class UCameraComponent;

UCLASS()
class COOPDEFENDER_API ACDCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACDCharacter();

protected:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	float BaseLookUpRate;
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Character")
	bool IsAiming;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character", meta = (ClampMin = 10.0f, ClampMax = 120.0f))
	float DefaultFieldOfView;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character", meta = (ClampMin = 10.0f, ClampMax = 120.0f))
	float AimFieldOfView;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character", meta = (ClampMin = 0.1f, ClampMax = 100.0f))
	float AimTransitionSpeed;
	UPROPERTY(BlueprintReadOnly, Category = "Character")
	float CurrentFieldOfView;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character")
	TMap<EAmmoType, int32> AmmoInventory;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character")
	TSubclassOf<ACDWeapon> StartingWeaponClass;
	UPROPERTY(BlueprintReadOnly, Category = "Character")
	ACDWeapon* CurrentWeapon;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Character")
	bool IsFiring;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	bool Dead;
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	UCDHealthComponent* HealthComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Character")
	void MoveForward(float Value);
	UFUNCTION(BlueprintCallable, Category = "Character")
	void MoveRight(float Value);
	UFUNCTION(BlueprintCallable, Category = "Character")
	void LookUp(float Rate);
	UFUNCTION(BlueprintCallable, Category = "Character")
	void Turn(float Rate);
	UFUNCTION(BlueprintCallable, Category = "Character")
	void BeginCrouch();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void EndCrouch();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void Fire();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void StopFire();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void Aim();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void StopAiming();
	UFUNCTION(BlueprintCallable, Category = "Character")
	void Reload();
	UFUNCTION()
	void OnHealthChanged(class UCDHealthComponent* HealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);


public:
	UFUNCTION(BlueprintCallable, Category = "Character")
	int32 GetAmmoFromInventory(EAmmoType AmmoType) const;
	UFUNCTION(BlueprintCallable, Category = "Character")
	void SetAmmoToInventory(EAmmoType AmmoType, int32 NewValue);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

};
