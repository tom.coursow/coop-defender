// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CoopDefenderGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class COOPDEFENDER_API ACoopDefenderGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
