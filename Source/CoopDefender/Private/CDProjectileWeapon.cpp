// Fill out your copyright notice in the Description page of Project Settings.


#include "CDProjectileWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

ACDProjectileWeapon::ACDProjectileWeapon()
{
	
}

void ACDProjectileWeapon::OnFire_Implementation(AActor* WeaponOwner, FVector EyeLocation, FRotator EyeRotation, FVector AimLocation)
{
	if (ProjectileClass)
	{
		FVector MuzzleLocation = GetMuzzleSocketLocation();
		FVector FireDirection = (AimLocation - MuzzleLocation);
		
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = GetInstigator();
		SpawnParams.Owner = this;
		
		GetWorld()->SpawnActor<ACDProjectile>(ProjectileClass->GetDefaultObject()->GetClass(), MuzzleLocation, FireDirection.Rotation(), SpawnParams);
	}	
}
