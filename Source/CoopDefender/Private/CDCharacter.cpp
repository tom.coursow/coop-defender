// Fill out your copyright notice in the Description page of Project Settings.


#include "CDCharacter.h"
#include "Components/InputComponent.h"
#include "Engine/World.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "CDWeapon.h"
#include "Components/CapsuleComponent.h"
#include "CoopDefender.h"
#include "Components/CDHealthComponent.h"

// Sets default values
ACDCharacter::ACDCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BaseTurnRate = 45.0f;
	BaseLookUpRate = 45.0f;
	IsAiming = false;
	AimFieldOfView = 45.0f;
	DefaultFieldOfView = 90.0f;
	AimTransitionSpeed = 20.0f;
	CurrentFieldOfView = DefaultFieldOfView;
	CurrentWeapon = nullptr;
	AmmoInventory = TMap<EAmmoType, int32>();
	Dead = false;

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -88.0f), FRotator(0.0f, -90.0f, 0.0f));

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f));
	SpringArmComponent->bUsePawnControlRotation = true;
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetFieldOfView(DefaultFieldOfView);
	HealthComponent = CreateDefaultSubobject<UCDHealthComponent>(TEXT("HealthComponent"));

	SpringArmComponent->SetupAttachment(RootComponent);
	CameraComponent->SetupAttachment(SpringArmComponent);
}

// Called when the game starts or when spawned
void ACDCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (StartingWeaponClass)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = this;
		SpawnParams.Owner = this;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FAttachmentTransformRules AttachRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
		CurrentWeapon = GetWorld()->SpawnActor<ACDWeapon>(StartingWeaponClass->GetDefaultObject()->GetClass(), FTransform(), SpawnParams);
		CurrentWeapon->AttachToComponent(GetMesh(), AttachRules, TEXT("weapon_socket"));
	}

	HealthComponent->OnHealthChanged.AddDynamic(this, &ACDCharacter::OnHealthChanged);
}

// Called every frame
void ACDCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurrentFieldOfView = FMath::FInterpTo(CurrentFieldOfView, IsAiming ? AimFieldOfView : DefaultFieldOfView, DeltaTime, AimTransitionSpeed);
	CameraComponent->SetFieldOfView(CurrentFieldOfView);

	if (IsFiring && CurrentWeapon)
	{
		CurrentWeapon->Fire();
		if (!CurrentWeapon->IsAutomatic())
		{
			IsFiring = false;
		}
	}
}

// Called to bind functionality to input
void ACDCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACDCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACDCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ACDCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &ACDCharacter::Turn);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ACDCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ACDCharacter::EndCrouch);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACDCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACDCharacter::StopJumping);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACDCharacter::Fire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ACDCharacter::StopFire);
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ACDCharacter::Aim);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ACDCharacter::StopAiming);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ACDCharacter::Reload);
}

FVector ACDCharacter::GetPawnViewLocation() const
{
	if (!CameraComponent)
	{
		return Super::GetPawnViewLocation();
	}
	return CameraComponent->GetComponentLocation();
}

void ACDCharacter::MoveForward(float Value)
{
	if (Value != 0.f)
	{
		AddMovementInput(GetActorForwardVector() * Value);
	}
}

void ACDCharacter::MoveRight(float Value)
{
	if (Value != 0.f)
	{
		AddMovementInput(GetActorRightVector() * Value);
	}
}

void ACDCharacter::LookUp(float Rate)
{
	if (Rate != 0.f)
	{
		AddControllerPitchInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds() * CustomTimeDilation);
	}
}

void ACDCharacter::Turn(float Rate)
{
	if (Rate != 0.f)
	{
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds() * CustomTimeDilation);
	}
}

void ACDCharacter::BeginCrouch()
{
	Crouch();
}

void ACDCharacter::EndCrouch()
{
	UnCrouch();
}

void ACDCharacter::Fire()
{
	IsFiring = true;
}

void ACDCharacter::StopFire()
{
	IsFiring = false;
}

void ACDCharacter::Aim()
{
	IsAiming = true;
}

void ACDCharacter::StopAiming()
{
	IsAiming = false;
}

void ACDCharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Reload();
	}
}

void ACDCharacter::OnHealthChanged(UCDHealthComponent* HealthComp, float Health, float HealthDelta, const UDamageType* DamageType,
                                   AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.0f && !Dead)
	{
		// DIE!
		Dead = true;
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.0f);
	}
}

int32 ACDCharacter::GetAmmoFromInventory(EAmmoType AmmoType) const
{
	const int* ammo = AmmoInventory.Find(AmmoType);
	if (!ammo)
	{
		return 0;
	}
	return *ammo;
}

void ACDCharacter::SetAmmoToInventory(EAmmoType AmmoType, int32 NewValue)
{
	AmmoInventory.Add(AmmoType, NewValue);
}
