// Fill out your copyright notice in the Description page of Project Settings.


#include "Public/CDWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "CoopDefender.h"
#include "CDCharacter.h"

static int32 DebugWeaponsConsoleParam = 0;
FAutoConsoleVariableRef CVARDebugWeapons(TEXT("CD.DebugWeapons"), DebugWeaponsConsoleParam, TEXT("Draws extra debug collisions and traces for weapons"), ECVF_Cheat);

// Sets default values
ACDWeapon::ACDWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	MuzzleSocketName = TEXT("muzzle_socket");
	FireCooldown = 1.0f;
	CurrentFireCooldown = 0.0f;
	Automatic = false;
	BaseDamage = 20.0f;
	WeakPointDamageMultiplier = 3.0f;
	AmmoMagazineSize = 60;
	AmmoType = EAmmoType::NONE;
	ReloadTime = 2.0f;
	CurrentReloadTime = 0.0f;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));

	SetRootComponent(SceneRoot);
	SkeletalMesh->SetupAttachment(SceneRoot);
}

void ACDWeapon::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = AmmoMagazineSize;
}

void ACDWeapon::UpdateReload()
{
	float PreviousReloadTime = CurrentReloadTime;
	CurrentReloadTime = FMath::Max(CurrentReloadTime - GetWorld()->DeltaTimeSeconds, 0.0f);
	if (CurrentReloadTime != PreviousReloadTime && CurrentReloadTime <= 0.0f)
	{
		OnReloadFinished();
	}
}

void ACDWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CurrentFireCooldown = FMath::Max(CurrentFireCooldown - DeltaSeconds, 0.0f);
	UpdateReload();
}

void ACDWeapon::Fire()
{
	if (CurrentFireCooldown > 0.0f || CurrentReloadTime > 0.0f)
	{
		return;
	}
	if (AmmoType != EAmmoType::NONE && CurrentAmmo <= 0)
	{
		return;
	}
	// Trace the World from Pawn Eyes to Crosshair Location
	AActor* WeaponOwner = GetOwner();
	if (WeaponOwner)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		GetInstigator()->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		// Hanlde Weapon Specific Fire Logic in Blueprint or C++ Implementations
		OnFire(WeaponOwner, EyeLocation, EyeRotation, GetAimLocation(EyeLocation, EyeRotation));
		CurrentFireCooldown = FireCooldown;
		CurrentAmmo--;

		PlayFireEffects();
	}
}

void ACDWeapon::OnFire_Implementation(AActor* WeaponOwner, FVector EyeLocation, FRotator EyeRotation, FVector AimLocation)
{
	FVector MuzzleLocation = GetMuzzleSocketLocation();
	FCollisionQueryParams TraceParams;
	TraceParams.AddIgnoredActor(WeaponOwner);
	TraceParams.AddIgnoredActor(this);
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.bTraceComplex = true;

	FHitResult Hit;
	bool hasHit = GetWorld()->LineTraceSingleByChannel(Hit, MuzzleLocation, AimLocation, COLLISION_WEAPON, TraceParams);
	if (hasHit)
	{
		// Hit - Process Damage
		AActor* ActorHit = Hit.GetActor();
		EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());
		float Damage = BaseDamage;
		if (SurfaceType == SURFACE_FLESHVULERABLE)
		{
			Damage *= WeakPointDamageMultiplier;
		}
		
		UGameplayStatics::ApplyPointDamage(ActorHit, Damage, AimLocation - MuzzleLocation, Hit, WeaponOwner->GetInstigatorController(), this, DamageType);
		PlayHitEffects(SurfaceType, Hit.ImpactPoint);
	}
	if (TracerEffect)
	{
		UParticleSystemComponent* TracerParticleSystem = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		TracerParticleSystem->SetVectorParameter(TEXT("BeamEnd"), AimLocation);
	}

	if (DebugWeaponsConsoleParam > 0)
	{
		DrawDebugLine(GetWorld(), MuzzleLocation, AimLocation, hasHit ? FColor::Red : FColor::Orange, false, 1.0f, 0, 1.0f);
	}
}

void ACDWeapon::Reload()
{
	auto OwnerCharacter = Cast<ACDCharacter>(GetOwner());
	if (!OwnerCharacter)
		return;

	int32 availableAmmo = OwnerCharacter->GetAmmoFromInventory(GetAmmoType());
	if (CurrentReloadTime > 0.0f || CurrentAmmo >= AmmoMagazineSize || CurrentAmmo <= 0 || availableAmmo <= 0)
	{
		return;
	}
	CurrentReloadTime = ReloadTime;
}

void ACDWeapon::OnReloadFinished()
{
	auto OwnerCharacter = Cast<ACDCharacter>(GetOwner());
	if (!OwnerCharacter)
		return;

	int32 availableAmmo = OwnerCharacter->GetAmmoFromInventory(GetAmmoType());
	int32 requiredAmmo = GetAmmoMagazineSize() - GetCurrentAmmo();
	int32 ammoLeft = availableAmmo - requiredAmmo;
	if (requiredAmmo > 0 && availableAmmo > 0)
	{
		SetCurrentAmmo(FMath::Min(GetCurrentAmmo() + availableAmmo, GetAmmoMagazineSize()));
		OwnerCharacter->SetAmmoToInventory(GetAmmoType(), FMath::Max(0, ammoLeft));
	}
}

void ACDWeapon::PlayFireEffects()
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, SkeletalMesh, MuzzleSocketName);
	}
	if (FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation(), 0.5f);
	}
	APlayerController* InstigatorController = Cast<APlayerController>(GetInstigatorController());
	if (InstigatorController)
	{
		InstigatorController->ClientPlayCameraShake(FireShake);
	}
}

void ACDWeapon::PlayHitEffects(EPhysicalSurface SurfaceType, FVector ImpactLocation)
{
	UParticleSystem* HitEffect = nullptr;
	switch (SurfaceType)
	{
	case SURFACE_FLESHDEFAULT:
		HitEffect = FleshDefaultHitEffect;
		break;
	case SURFACE_FLESHVULERABLE:
		HitEffect = FleshVulnerableHitEffect;
		break;
	default:
		HitEffect = DefaultHitEffect;
	}
	if (HitEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, ImpactLocation);
	}
}

FVector ACDWeapon::GetAimLocation(FVector EyeLocation, FRotator EyeRotation)
{
	AActor* WeaponOwner = GetOwner();
	FVector AimLocation = EyeLocation + (EyeRotation.Vector() * 10000);
	FCollisionQueryParams TraceParams;
	TraceParams.AddIgnoredActor(WeaponOwner);
	TraceParams.AddIgnoredActor(this);
	TraceParams.bTraceComplex = true;
	FHitResult Hit;
	bool hasHit = GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, AimLocation, COLLISION_WEAPON, TraceParams);
	if (hasHit)
	{
		AimLocation = Hit.ImpactPoint + EyeRotation.Vector();
	}
	return AimLocation;
}

FVector ACDWeapon::GetMuzzleSocketLocation()
{
	if (!SkeletalMesh)
	{
		return FVector(0.0f);
	}
	return SkeletalMesh->GetSocketLocation(TEXT("muzzle_socket"));
}

bool ACDWeapon::IsAutomatic() const
{
	return Automatic;
}

EAmmoType ACDWeapon::GetAmmoType() const
{
	return AmmoType;
}

int32 ACDWeapon::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

void ACDWeapon::SetCurrentAmmo(int32 NewAmmo)
{
	this->CurrentAmmo = FMath::Min(NewAmmo, AmmoMagazineSize);
}

int32 ACDWeapon::GetAmmoMagazineSize() const
{
	return AmmoMagazineSize;
}

bool ACDWeapon::IsReloading() const
{
	return CurrentReloadTime > 0;
}
