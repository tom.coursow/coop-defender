// Fill out your copyright notice in the Description page of Project Settings.


#include "CDProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundBase.h"

// Sets default values
ACDProjectile::ACDProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileCollision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
	ProjectileHitCollision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileHitCollision"));
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	SetRootComponent(ProjectileCollision);
	ProjectileHitCollision->SetupAttachment(ProjectileCollision);
	ProjectileMesh->SetupAttachment(ProjectileHitCollision);
}

// Called when the game starts or when spawned
void ACDProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACDProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

