// Fill out your copyright notice in the Description page of Project Settings.


#include "CDExplodable.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CDHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ACDExplodable::ACDExplodable()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	ExplosionArea = CreateDefaultSubobject<USphereComponent>(TEXT("ExplosionArea"));
	HealthComponent = CreateDefaultSubobject<UCDHealthComponent>(TEXT("HealthComponent"));
	
	ExplosionArea->SetSphereRadius(300.0f);
	ExplosionArea->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ExplosionArea->SetCollisionResponseToAllChannels(ECR_Ignore);
	ExplosionArea->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	ExplosionArea->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Overlap);

	if (DefaultMaterial)
	{
		Mesh->SetMaterial(0, DefaultMaterial);
	}
	
	SetRootComponent(Mesh);
	ExplosionArea->SetupAttachment(Mesh);

	Exploded = false;
	ExplodeOnDamage = true;
	ExplosionForce = 300.0f;
	ExplosionDamage = 0.0f;
}

// Called when the game starts or when spawned
void ACDExplodable::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnHealthChanged.AddDynamic(this, &ACDExplodable::OnHealthChanged);
	ExplosionArea->OnComponentBeginOverlap.AddDynamic(this, &ACDExplodable::OnExplosionAreaBeginOverlap);
}

void ACDExplodable::OnHealthChanged(UCDHealthComponent* HealthComp, float Health, float HealthDelta, const UDamageType* DamageTypeSettings, AController* InstigatedBy,
	AActor* DamageCauser)
{
	if (Health <= 0.0f && ExplodeOnDamage && !Exploded)
	{
		Explode();
	}
}

void ACDExplodable::Explode()
{
	Exploded = true;
	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
	}
	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());
	}
	ExplosionArea->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	if (ExplodedMaterial)
	{
		Mesh->SetMaterial(0, ExplodedMaterial);
	}
	
	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(ExplosionArea->GetScaledSphereRadius());
	TArray<FOverlapResult> OverlapResults;
	GetWorld()->OverlapMultiByChannel(OverlapResults, ExplosionArea->GetComponentLocation(), FQuat::Identity, ECC_PhysicsBody, CollisionShape);
	DrawDebugSphere(GetWorld(), ExplosionArea->GetComponentLocation(), ExplosionArea->GetScaledSphereRadius(), 50, FColor::Red, false, 3.0f);
	for (int i = 0; i < OverlapResults.Num(); i++)
	{
		FOverlapResult OverlapResult = OverlapResults[i];
		OverlapResult.Component->AddRadialImpulse(ExplosionArea->GetComponentLocation(), ExplosionArea->GetScaledSphereRadius(), ExplosionForce, RIF_Constant, true);
		TSubclassOf<UCDHealthComponent> HealthComponentClass = UCDHealthComponent::StaticClass();
		UCDHealthComponent* HealthComp = Cast<UCDHealthComponent>(OverlapResult.Actor->GetComponentByClass(HealthComponentClass));
		if (HealthComp && ExplosionDamage >= 0.0f)
		{
			HealthComp->Damage(ExplosionDamage, DamageType, GetInstigatorController(), this);
		}
	}
}

void ACDExplodable::OnExplosionAreaBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}
