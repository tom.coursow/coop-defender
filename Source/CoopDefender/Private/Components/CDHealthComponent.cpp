// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CDHealthComponent.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UCDHealthComponent::UCDHealthComponent()
{
	MaxHealth = 100.0f;
	Health = 100.0f;
}


void UCDHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);

	OnHealthChanged.Broadcast(this, Health, Damage, DamageType, InstigatedBy, DamageCauser);
}

void UCDHealthComponent::Damage(float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage > 0.0f)
	{
		OnTakeAnyDamage(GetOwner(), Damage, DamageType, InstigatedBy, DamageCauser);
	}
}

// Called when the game starts
void UCDHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();
	if (Owner)
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UCDHealthComponent::OnTakeAnyDamage);
	}
}
