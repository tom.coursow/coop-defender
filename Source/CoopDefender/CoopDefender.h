// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// Custom Physics Surface Types
#define SURFACE_FLESHDEFAULT	SurfaceType1
#define SURFACE_FLESHVULERABLE	SurfaceType2

// Custom Trace Collision Channels
#define COLLISION_WEAPON		ECC_GameTraceChannel1